with import (
  fetchTarball https://github.com/NixOS/nixpkgs/archive/18.09-beta.tar.gz
) {};
#with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "latex-env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    tectonic
  ];
}
